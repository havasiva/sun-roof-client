# Sunroof Client side

This is a client side for project [sunroof](https://gitlab.fit.cvut.cz/havasiva/sun-roof).
It is a console app.

## Installation

Make sure you have [server side](https://gitlab.fit.cvut.cz/havasiva/sun-roof) already downloaded and running.

Now you can download this repository and install it using

`mvn install`

## Starting

To start this application you need to run JAR file.

`java -jar target\sunroof-client-1.0.jar`

## Using

### Entity

This project will allow you to use CRUD functions on one entity - *Employee* by using option

`--entity=employee`

Plus an additional operation on entity *Order*

### Actions

Current list of actions is
- create
- update
- delete
- readOne
- readAll

`--action=[action]`

#### create

Create takes additional options
- firstName
- lastName
- mail

`--[option]=[your_data]`

Creates new employee.

#### update

Update takes additional options
- id
- firstName
- lastName
- mail

id must already exist.

`--[option]=[your_data]`

Updates employee by given id.

#### delete

Delete takes single option
- id

`--id=[your_data]`

Deletes employee by given id.

#### readOne

ReadOne takes single option
- id

`--id=[your_data]`

Reads employee by given id.

#### readAll

Takes no options.

Reads all employees.

#### findByName

FindByName takes single option
- name

Finds all employees with the same first name.

`--name=[your_data]`

### Entity order

#### Actions

#### updateALL

Updates all orders with given name and made after certain dte to a set price.

UpdateAll takes additional options
- name
- price
- date

`--[option]="[your_data]"`

Example

`java -jar target\sunroof-client-1.0.jar --entity=order --action=updateAll --name="Double Solar Panel v1" --price="999.99" --date="2020-02-02"`

### Example

`java -jar target\sunroof-client-1.0.jar --action=readOne --entity=employee --id=10`

will return information about employee with id = 10


