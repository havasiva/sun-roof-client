package cz.cvut.fit.havasiva.sunroofclient;

import net.minidev.json.parser.JSONParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Component
public class EmployeeResource {

    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .setConnectTimeout(Duration.ofMillis(3000))
                .setReadTimeout(Duration.ofMillis(3000))
                .build();
    }

    HttpHeaders headers = new HttpHeaders();

    public void create(String firstName, String lastName, String mail) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{\"firstName\":\""+ firstName+"\", \"lastName\": \""+lastName+"\", \"mail\": \""+ mail+"\"}", headers);
        try {
            restTemplate.postForLocation("http://localhost:8080/employee", entity);
            System.out.println("Created successfully!");
        } catch (Exception e) {
            System.out.println("Error while creating");
            System.out.println(e.getMessage());
        }
    }

    public void update(int id, String firstName, String lastName, String mail) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{\"firstName\":\""+ firstName+"\", \"lastName\": \""+lastName+"\", \"mail\": \""+ mail+"\"}", headers);
        try {
            restTemplate.put("http://localhost:8080/employee/" + id , entity);
            System.out.println("Updated successfully!");
        } catch (Exception e) {
            System.out.println("Error while updating");
            System.out.println(e.getMessage());
        }
    }

    public void delete(int id) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{\"id\":\""+ id+"\"", headers);
        try {
            restTemplate.delete("http://localhost:8080/employee/" + id , entity);
            System.out.println("Deleted successfully!");
        } catch (Exception e) {
            System.out.println("Error while deleting");
            System.out.println(e.getMessage());
        }
    }

    public void readAll() {
        try {
            ResponseEntity<String> response = restTemplate.exchange("http://localhost:8080/employee/all", HttpMethod.GET, null, String.class);
            JSONArray json = new JSONArray(response.getBody());
            if(json.length() > 0)
            {
                System.out.println("===========================");
                for(int i = 0; i < json.length(); i++)
                {
                    System.out.println("id: " + json.getJSONObject(i).get("id").toString());
                    System.out.println("firstName: " + json.getJSONObject(i).get("firstName").toString());
                    System.out.println("lastName: " + json.getJSONObject(i).get("lastName").toString());
                    System.out.println("mail: " + json.getJSONObject(i).get("mail").toString());
                    System.out.println("===========================");
                }
            } else {
                System.out.println("No data found!");
            }
        } catch (Exception e) {
            System.out.println("Error while reading all");
            System.out.println(e.getMessage());
        }
    }

    public void readOne(int id) {
        try {
            ResponseEntity<String> response = restTemplate.exchange("http://localhost:8080/employee/" + id, HttpMethod.GET, null, String.class);
            JSONObject json = new JSONObject(response.getBody());
            System.out.println("Read successfully!");
            System.out.println("id: " + json.get("id"));
            System.out.println("firstName: " + json.get("firstName"));
            System.out.println("lastName: " + json.get("lastName"));
            System.out.println("mail: " + json.get("mail"));
        } catch (Exception e) {
            System.out.println("Error while reading");
            System.out.println(e.getMessage());
        }
    }

    public void findByFirstName(String firstName) {
        try {
            ResponseEntity<String> response = restTemplate.exchange("http://localhost:8080/employee/name/" + firstName, HttpMethod.GET, null, String.class);
            JSONArray json = new JSONArray(response.getBody());
            if(json.length() > 0)
            {
                System.out.println("===========================");
                for(int i = 0; i < json.length(); i++)
                {
                    System.out.println("id: " + json.getJSONObject(i).get("id").toString());
                    System.out.println("firstName: " + json.getJSONObject(i).get("firstName").toString());
                    System.out.println("lastName: " + json.getJSONObject(i).get("lastName").toString());
                    System.out.println("mail: " + json.getJSONObject(i).get("mail").toString());
                    System.out.println("===========================");
                }
            } else {
                System.out.println("No data found!");
            }
        } catch (Exception e) {
            System.out.println("Error while reading");
            System.out.println(e.getMessage());
        }
    }

}
