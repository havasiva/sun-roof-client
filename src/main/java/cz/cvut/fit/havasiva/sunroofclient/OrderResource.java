package cz.cvut.fit.havasiva.sunroofclient;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.List;

@Component
public class OrderResource {

    @Autowired
    private RestTemplate restTemplateOrder;

    @Bean
    public RestTemplate restTemplateOrder(RestTemplateBuilder builder) {
        return builder
                .setConnectTimeout(Duration.ofMillis(3000))
                .setReadTimeout(Duration.ofMillis(3000))
                .build();
    }

    public void updateAll(String name, String price, String date) {
        try {
            ResponseEntity<String> response = restTemplateOrder.exchange("http://localhost:8080/order/update/"+ name +"/" + price + "/"+ date, HttpMethod.PUT, null, String.class);
            System.out.println("Updated successfully!");
            System.out.println("Updated ids: " + response.getBody());
        } catch (Exception e) {
            System.out.println("Error while updating all orders!");
            System.out.println(e.getMessage());
        }
    }

}
