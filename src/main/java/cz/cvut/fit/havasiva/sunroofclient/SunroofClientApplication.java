package cz.cvut.fit.havasiva.sunroofclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SunroofClientApplication implements ApplicationRunner {

    @Autowired
    EmployeeResource employeeResource;

    @Autowired
    OrderResource orderResource;

    public static void main(String[] args) {
        SpringApplication.run(SunroofClientApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (args.containsOption("action") && args.containsOption("entity")) {
            System.out.println("Starting Client Application");
            switch(args.getOptionValues("entity").get(0)) {
                case "employee":
                    switch (args.getOptionValues("action").get(0)) {
                        case "create":
                            if (args.containsOption("firstName") && args.containsOption("lastName") && args.containsOption("mail")) {
                                employeeResource.create(args.getOptionValues("firstName").get(0), args.getOptionValues("lastName").get(0), args.getOptionValues("mail").get(0));
                            } else {
                                System.err.println("[Err] - Create: Employee: You are missing argument options!");
                            }
                            break;
                        case "update":
                            if (args.containsOption("firstName") && args.containsOption("lastName") && args.containsOption("mail") && args.containsOption("id")) {
                                employeeResource.update(Integer.parseInt(args.getOptionValues("id").get(0)), args.getOptionValues("firstName").get(0), args.getOptionValues("lastName").get(0), args.getOptionValues("mail").get(0));
                            } else {
                                System.err.println("[Err] - Update: Employee: You are missing argument options!");
                            }
                            break;
                        case "readAll":
                            employeeResource.readAll();
                            break;
                        case "readOne":
                            if (args.containsOption("id")) {
                                employeeResource.readOne(Integer.parseInt(args.getOptionValues("id").get(0)));
                            } else {
                                System.err.println("[Err] - readOne: Employee: You are missing argument options!");
                            }
                            break;
                        case "delete":
                            if (args.containsOption("id")) {
                                employeeResource.delete(Integer.parseInt(args.getOptionValues("id").get(0)));
                            } else {
                                System.err.println("[Err] - Delete: Employee: You are missing argument options!");
                            }
                            break;
                        case "findByName":
                            if (args.containsOption("name")) {
                                employeeResource.findByFirstName(args.getOptionValues("name").get(0));
                            } else {
                                System.err.println("[Err] - findByName: Employee: You are missing argument options!");
                            }
                            break;
                        default:
                            System.err.println("[Err] - Wrong Action Option! " + args.getOptionValues("action").get(0) + " doesn't exist!");
                            break;
                    }
                    break;
                case "order":
                    if (args.containsOption("name") && args.containsOption("price") && args.containsOption("date")) {
                        switch(args.getOptionValues("action").get(0)) {
                            case "updateAll":
                                orderResource.updateAll(args.getOptionValues("name").get(0), args.getOptionValues("price").get(0), args.getOptionValues("date").get(0));
                                break;
                            default:
                                System.err.println("[Err] - Wrong Action Option! " + args.getOptionValues("action").get(0) + " doesn't exist!");
                                break;
                        }
                    } else {
                        System.err.println("[Err] - Order - missing option!");
                    }
                    break;
                default:
                    System.err.println("No entity found!");
                    break;
            }

            System.out.println("Ending session");
        } else
        {
            System.out.println("Missing Options: action or entity");
        }
    }
}

